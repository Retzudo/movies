from functools import lru_cache

import requests
from django.conf import settings

BASE_URL = "https://www.omdbapi.com/"


class OmdbError(Exception):
    pass


@lru_cache
def get_movie(imdb_id: str):
    response = requests.get(
        BASE_URL,
        params={
            "apikey": settings.OMDB_API_KEY,
            "type": "movie",
            "tomatoes": "true",
            "i": imdb_id,
        },
    )

    json = response.json()

    if json.get("Response") == "False":
        raise OmdbError(json.get("Error"))

    return json


@lru_cache
def find_movies(title: str):
    response = requests.get(
        BASE_URL,
        params={
            "apikey": settings.OMDB_API_KEY,
            "type": "movie",
            "s": title,
        },
    )

    json = response.json()

    if json.get("Response") == "False":
        return []

    return json.get("Search")
