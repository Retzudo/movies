from typing import Optional

from django.core import validators
from django.db import models


class Movie(models.Model):
    imdb_id = models.CharField(max_length=10, unique=True, primary_key=True)
    watched = models.BooleanField(default=False)
    date_added = models.DateTimeField(auto_now_add=True)
    suggested_by = models.CharField(max_length=100, verbose_name="Vorgeschlagen von")
    poop_score = models.SmallIntegerField(
        validators=[validators.MinValueValidator(0), validators.MaxValueValidator(5)],
        default=0,
        verbose_name="Trash/5",
    )
    comment = models.TextField(
        null=True,
        blank=True,
        verbose_name="Kommentar",
    )
    data = models.JSONField()

    def __str__(self):
        title = self.data.get("Title")
        year = self.data.get("Year")

        return f"{title} ({year})"

    class Meta:  # pyright: ignore[reportIncompatibleVariableOverride]
        ordering = ("date_added",)

    @property
    def title(self):
        return self.data.get("Title")

    @property
    def imdb_rating(self) -> Optional[float]:
        try:
            return float(self._get_rating("Internet Movie Database").replace("/10", ""))
        except ValueError:
            return None

    @property
    def rotten_tomatoes_rating(self) -> Optional[int]:
        try:
            return int(self._get_rating("Rotten Tomatoes").replace("%", ""))
        except ValueError:
            return None

    @property
    def metacritic_rating(self) -> Optional[int]:
        try:
            return int(self._get_rating("Metacritic").replace("/100", ""))
        except ValueError:
            return None

    def _get_rating(self, source: str) -> str:
        ratings = self.data.get("Ratings", [])

        for rating in ratings:
            if rating.get("Source") == source:
                return rating.get("Value", "0")

        return "N/A"

    @property
    def runtime(self) -> Optional[int]:
        try:
            minutes, *_ = self.data.get("Runtime", "").split()
            return int(minutes)
        except ValueError:
            return None


class WatchParty(models.Model):
    movie = models.ForeignKey(
        Movie, on_delete=models.PROTECT, null=True, verbose_name="Film"
    )
    datetime = models.DateTimeField()

    def __str__(self):
        if self.movie:
            return f'Filmabend: {self.movie.data.get("Title")}'
        else:
            return "Filmabend"

    class Meta:  # pyright: ignore[reportIncompatibleVariableOverride]
        verbose_name_plural = "Watch Parties"
        ordering = ("-datetime",)


class Person(models.Model):
    name = models.TextField()

    class Meta:
        verbose_name_plural = "People"
        ordering = ("name",)

    def __str__(self):
        return self.name
