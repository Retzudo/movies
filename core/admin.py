from django.contrib import admin

from core import models

admin.site.register(models.Movie)
admin.site.register(models.WatchParty)
admin.site.register(models.Person)
