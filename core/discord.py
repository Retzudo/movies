import requests
from datetime import datetime

from django.conf import settings


def activate_hook(when: datetime, imdb_url: str):
    timestamp = when.timestamp()
    role_id = settings.DISCORD_PING_ROLE_ID
    webhook_url = settings.DISCORD_WEBHOOK_URL

    message = f"<@&{role_id}> <t:{timestamp:.0f}:F> (<t:{timestamp:.0f}:R>) {imdb_url}"

    requests.post(webhook_url, {"content": message})
