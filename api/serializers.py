from rest_framework import serializers

from core.models import Movie, WatchParty


class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = "__all__"


class WatchPartySerializer(serializers.ModelSerializer):
    class Meta:
        model = WatchParty
        fields = "__all__"
