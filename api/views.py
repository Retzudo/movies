import re

from rest_framework import viewsets
from rest_framework.exceptions import ParseError
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_api_key.permissions import HasAPIKey

from api.serializers import MovieSerializer, WatchPartySerializer
from core import omdb
from core.models import Movie, WatchParty


class MovieViewSet(viewsets.ModelViewSet):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
    permission_classes = [IsAuthenticated | HasAPIKey]

    def get_queryset(self):
        limit = self.request.query_params.get("limit", None)
        sort = self.request.query_params.get("sort", None)
        watched = self.request.query_params.get("watched", False)

        movies = self.queryset.filter(watched=bool(watched))

        if sort:
            if sort == "random":
                sort = "?"
            movies = movies.order_by(sort)

        if limit:
            try:
                return movies[:int(limit)]
            except ValueError:
                raise ParseError("Invalid limit")

        return movies


class WatchPartyViewSet(viewsets.ModelViewSet):
    queryset = WatchParty.objects.all()
    serializer_class = WatchPartySerializer
    permission_classes = [IsAuthenticated | HasAPIKey]


class SearchView(APIView):
    permission_classes = [IsAuthenticated | HasAPIKey]

    def get(self, request):
        query = request.GET.get("q", "")
        movies = []
        regex = re.compile(r"tt\d{7,8}")

        matches = regex.findall(query)

        if len(matches) == 1:
            imdb_id = matches[0]
            movies = [omdb.get_movie(imdb_id)]
        elif query:
            movies = omdb.find_movies(query)

        return Response(movies)
