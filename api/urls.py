from django.urls import include, path
from rest_framework import routers

from api import views

router = routers.DefaultRouter()
router.register("movies", views.MovieViewSet)
router.register("watchparties", views.WatchPartyViewSet)

urlpatterns = [
    path("", include(router.urls)),
    path("omdb/search/", views.SearchView.as_view()),
]
