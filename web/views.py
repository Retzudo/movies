import datetime
import random
import re
from pathlib import Path

from django.conf import settings
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.models import User
from django.http import Http404, HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, FormView
from icalendar import Calendar, Event, Alarm

from core import omdb, discord
from core.models import Movie, WatchParty, Person
from core.omdb import OmdbError
from web.forms import WatchPartyForm, LoginForm


class ListMovieView(LoginRequiredMixin, ListView):
    model = Movie

    def get_queryset(self):
        include_watched = bool(self.request.GET.get("includewatched"))
        results = super().get_queryset()

        if not include_watched:
            results = results.filter(watched=False)

        return results


class CreateMovieView(LoginRequiredMixin, CreateView):
    model = Movie
    fields = ["suggested_by", "poop_score", "comment"]
    success_url = reverse_lazy("list")

    def form_valid(self, form):
        imdb_id = self.kwargs.get("pk")
        data = omdb.get_movie(imdb_id)
        movie = form.save(commit=False)
        movie.imdb_id = imdb_id
        movie.data = data
        movie.save()

        return super().form_valid(form)


class DeleteMovieView(LoginRequiredMixin, DeleteView):
    model = Movie
    success_url = reverse_lazy("list")


class UpdateMovieView(LoginRequiredMixin, UpdateView):
    model = Movie
    fields = ["suggested_by", "poop_score", "comment", "watched"]
    success_url = reverse_lazy("list")


class ListWatchPartyView(LoginRequiredMixin, ListView):
    model = WatchParty

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)

        context["past"] = WatchParty.objects.filter(
            datetime__lt=datetime.datetime.now()
        )
        context["future"] = WatchParty.objects.filter(
            datetime__gte=datetime.datetime.now()
        )
        context["next"] = context["future"].first()

        return context


class CreateWatchPartyView(PermissionRequiredMixin, FormView):
    permission_required = "core.add_watchparty"
    form_class = WatchPartyForm
    template_name = "core/watchparty_form.html"
    success_url = reverse_lazy("parties")

    def get_initial(self):
        initial = super().get_initial()
        imdb_id = self.request.GET.get("imdbId")

        if imdb_id:
            movie = Movie.objects.get(pk=imdb_id)
            if movie:
                initial["movie"] = movie

        return initial

    def form_valid(self, form):
        movie = form.cleaned_data.get("movie")
        date = form.cleaned_data.get("date")
        time = form.cleaned_data.get("time")
        when = datetime.datetime.combine(date, time)

        discord.activate_hook(when, f"https://www.imdb.com/title/{movie.imdb_id}/")

        WatchParty.objects.create(
            movie=movie,
            datetime=when,
        )

        return super().form_valid(form)


class LoginView(FormView):
    form_class = LoginForm
    template_name = "web/login_form.html"
    success_url = reverse_lazy("list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        gifs = [path.name for path in Path("web/static/web/gifs/").glob("*.gif")]

        try:
            gif = random.choice(gifs)
            context["random_gif"] = f"web/gifs/{gif}"
        except IndexError:
            pass

        return context

    def form_valid(self, form):
        # Create the default user if it doesn't exist
        user, _ = User.objects.get_or_create(username=settings.DEFAULT_USERNAME)

        # Update the user's password if it doesn't match the configured one
        if not user.check_password(settings.DEFAULT_PASSWORD):
            user.set_password(settings.DEFAULT_PASSWORD)
            user.save()

        user = authenticate(
            self.request,
            username=settings.DEFAULT_USERNAME,
            password=form.cleaned_data.get("password"),
        )

        if user:
            login(self.request, user)

        return super().form_valid(form)


@login_required
def omdb_get(request, imdb_id):
    try:
        movie = omdb.get_movie(imdb_id)
    except OmdbError:
        raise Http404(f"Movie with ID {imdb_id} could not be found.")

    in_db = Movie.objects.filter(data__imdbID=movie.get("imdbID")).exists()

    return render(
        request,
        "web/omdb_movie.html",
        context={
            "movie": movie,
            "in_db": in_db,
        },
    )


@login_required
def omdb_search(request):
    query = request.GET.get("q", "")
    movies = None
    regex = re.compile(r"tt\d{7,8}")

    matches = regex.findall(query)

    if len(matches) == 1:
        imdb_id = matches[0]
        movies = [omdb.get_movie(imdb_id)]
    elif query:
        movies = omdb.find_movies(query)

    movie_ids_in_list = Movie.objects.all().values_list("imdb_id", flat=True)

    return render(
        request,
        "web/omdb_search.html",
        context={
            "movies": movies,
            "movie_ids_in_list": movie_ids_in_list,
        },
    )


@login_required
def magic_conch_shell(request):
    random_movies = Movie.objects.filter(watched=False).order_by("?")[:3]

    return render(
        request,
        "web/magic_conch_shell.html",
        context={
            "random_movies": random_movies,
        },
    )


def party_ical(request):
    calendar = Calendar()
    calendar.add("prodid", "-//Filmabende//movies.retzudo.com//")
    calendar.add("version", "2.0")

    for party in WatchParty.objects.all():
        event = Event()
        movie_title = party.movie.data.get("Title")
        event.add("summary", f"Filmabend: {movie_title}")
        event.add("dtstart", party.datetime)
        event.add(
            "dtend",
            party.datetime + datetime.timedelta(minutes=party.movie.runtime or 120),
        )

        alarm = Alarm()
        alarm.add("trigger", datetime.timedelta(minutes=-10))
        event.add_component(alarm)

        calendar.add_component(event)

    return HttpResponse(calendar.to_ical(), content_type="text/calendar")


def movies_csv(request):
    movie_titles = list(
        Movie.objects.filter(watched=False).values_list("data__Title", flat=True)
    )
    people = [person.name for person in Person.objects.all()]
    additional_people = request.GET.getlist("person")

    return HttpResponse(
        "\n".join(movie_titles + people + additional_people), content_type="text/csv"
    )
