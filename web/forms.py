import datetime

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit
from django import forms

from core import models


def _today_plus_seven_days() -> str:
    today = datetime.date.today()
    next_week = today + datetime.timedelta(days=7)

    return next_week.isoformat()


class WatchPartyForm(forms.ModelForm):
    date = forms.DateField(
        widget=forms.DateInput(attrs={"type": "date"}),
        label="Datum",
        initial=_today_plus_seven_days,
    )
    time = forms.TimeField(
        widget=forms.TimeInput(attrs={"type": "time"}),
        label="Uhrzeit",
        initial=datetime.time(hour=20, minute=15),
    )

    class Meta:
        model = models.WatchParty
        exclude = ["datetime"]


class LoginForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        self.helper.layout = Layout(
            Field("password", placeholder="Passwort"), Submit("log-in", "Einloggen")
        )
