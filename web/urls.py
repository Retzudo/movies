from django.contrib.auth.views import logout_then_login
from django.urls import path

from web import views

urlpatterns = [
    path("login", views.LoginView.as_view(), name="login"),
    path("logout", logout_then_login, name="logout"),
    path("", views.ListMovieView.as_view(), name="list"),
    path("movies.csv", views.movies_csv, name="movies_csv"),
    path("movies/add/<str:pk>", views.CreateMovieView.as_view(), name="add_movie"),
    path("movies/edit/<str:pk>", views.UpdateMovieView.as_view(), name="edit_movie"),
    path(
        "movies/delete/<str:pk>", views.DeleteMovieView.as_view(), name="delete_movie"
    ),
    path("movies/magic-conch-shell", views.magic_conch_shell, name="magic_conch_shell"),
    path("parties", views.ListWatchPartyView.as_view(), name="parties"),
    path("parties/create", views.CreateWatchPartyView.as_view(), name="create_party"),
    path("parties.ics", views.party_ical, name="party_ical"),
    path("omdb/search", views.omdb_search, name="omdb_search"),
    path("omdb/movie/<str:imdb_id>", views.omdb_get, name="omdb_get"),
]
